﻿using DevExpress.XtraCharts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EarthquakeSimulationAPP
{
    public partial class Form1 : Form
    {

        List<Datos> listaDatos = new List<Datos>();
        SerialPort serialPort = new SerialPort();
        DataTable datasource = new DataTable("Tabla");
        int cont = 50000;
        bool alarma = false;
        bool p1 = false, p2=false, p3=false, p4=false;
        SoundPlayer sound = new SoundPlayer(@"C:\sonido\alerta_sis.wav");

        public Form1()
        {
            InitializeComponent();
            configuraDataTable(); 
            configuraGrafico();            
            configuraPuerto();
            actualizaImagenes();
        }

        private void configuraDataTable()
        {            
            datasource.Columns.Add("Argument", typeof(Int32));
            datasource.Columns.Add("Value", typeof(Double));
            datasource.Columns.Add("Value2", typeof(Double));
            datasource.Columns.Add("Value3", typeof(Double));
            datasource.Columns.Add("Value4", typeof(Double));
        }

        

        private void configuraPuerto()
        {
            serialPort.BaudRate = 9600;
            serialPort.PortName = "COM3";

            serialPort.DataReceived += new SerialDataReceivedEventHandler(serialPortReceiver);

            try
            {
                serialPort.Open(); 
            }
            catch
            {
            }


        }

        

        private void configuraGrafico()
        {
            chartControl1.DataSource = datasource;
            Series series = chartControl1.Series[0];
            series.LegendText = "Edificio";
            //series.DataSource = CreateChartData(5);
            series.ArgumentScaleType = ScaleType.Auto;
            series.ArgumentDataMember = "Argument";
            series.ValueScaleType = ScaleType.Numerical;
            series.ValueDataMembers.AddRange(new string[] { "Value" });
            //
            Series series2 = chartControl1.Series[1];
            series2.LegendText = "Iglesia";
            //series2.DataSource = CreateChartData(5); 
            series2.ArgumentScaleType = ScaleType.Auto;
            series2.ArgumentDataMember = "Argument";
            series2.ValueScaleType = ScaleType.Numerical;
            series2.ValueDataMembers.AddRange(new string[] { "Value2" });
            ///
            Series series3 = chartControl1.Series[2];
            series3.LegendText = "Ovelisco";
            //series3.DataSource = CreateChartData(5); 
            series3.ArgumentScaleType = ScaleType.Auto;
            series3.ArgumentDataMember = "Argument";
            series3.ValueScaleType = ScaleType.Numerical;
            series3.ValueDataMembers.AddRange(new string[] { "Value3" });
            ///
            Series series4 = chartControl1.Series[3];
            series4.LegendText = "Edificio Externo";
            //series4.DataSource = CreateChartData(5);
            series4.ArgumentScaleType = ScaleType.Auto;
            series4.ArgumentDataMember = "Argument";
            series4.ValueScaleType = ScaleType.Numerical;
            series4.ValueDataMembers.AddRange(new string[] { "Value4" });
        }


        private DataTable CreateChartData(int rowCount)
        {
            // Create an empty table.
            DataTable table = new DataTable("Table1");

            // Add two columns to the table.
            table.Columns.Add("Argument", typeof(Int32));
            table.Columns.Add("Value", typeof(Double));
            table.Columns.Add("Value2", typeof(Double));
            table.Columns.Add("Value3", typeof(Double));
            table.Columns.Add("Value4", typeof(Double));

            // Add data rows to the table.            
            DataRow row = null;
            int col = 0; 
            foreach(Datos d in listaDatos)            
            {
                row = table.NewRow();
                row["Argument"] = col;
                row["Value"] = d.Sensor1; // rnd.Next(100);
                row["Value2"] = d.Sensor2;
                row["Value3"] = d.Sensor3;
                row["Value4"] = d.Sensor4;
                table.Rows.Add(row);                
                col++;
            }

            return table;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            DataRow row = datasource.NewRow();
            row["Argument"] = cont;
            cont--;
            row["Value"] =  rnd.Next(100);
            row["Value2"] = rnd.Next(100);
            row["Value3"] = rnd.Next(100);
            row["Value4"] = rnd.Next(100);
            if (datasource.Rows.Count > 40)
                datasource.Rows.RemoveAt(datasource.Rows.Count-1);
            datasource.Rows.InsertAt(row,0);
            chartControl1.Refresh();
        }

        

        private void serialPortReceiver(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort currentSerialPort = (SerialPort)sender;
            string datos = currentSerialPort.ReadLine();
            Console.WriteLine(datos);
            
            if(datos.Equals(""))
                return; 
            else {
                datos = datos.Split('#')[0];
                string[] val = datos.Split(',');
                Random rnd = new Random();
                DataRow row = datasource.NewRow();
                row["Argument"] = cont;
                cont--;
                row["Value"] = Convert.ToDouble(val[0]);
                p1 = Convert.ToDouble(val[0])>500;
                row["Value2"] = Convert.ToDouble(val[1]);
                p2 = Convert.ToDouble(val[1]) > 500;
                row["Value3"] = Convert.ToDouble(val[2]);
                p3 = Convert.ToDouble(val[2]) > 500;
                row["Value4"] = Convert.ToDouble(val[3]);
                p4 = Convert.ToDouble(val[3]) > 500;
                if (datasource.Rows.Count > 50)
                    datasource.Rows.RemoveAt(datasource.Rows.Count-1);
                datasource.Rows.InsertAt(row,0);
            
                }            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            chartControl1.Refresh();
            if (alarma)
                sound.Play();
            else
                sound.Stop();
            actualizaImagenes();
            
        }

        private void actualizaImagenes()
        {
            if (p1)
                pictureBox4.Visible = p1;
            if (p2)
                pictureBox5.Visible = p2;
            if (p3)
                pictureBox2.Visible = p3;
            if (p4)
                pictureBox3.Visible = p4;
            if (p1 || p2 || p3 || p4)
                alarma = true;
            else
                alarma = false;
        }

    }
}
