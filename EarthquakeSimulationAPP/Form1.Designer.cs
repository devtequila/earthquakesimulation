﻿namespace EarthquakeSimulationAPP
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraCharts.SwiftPlotDiagram swiftPlotDiagram7 = new DevExpress.XtraCharts.SwiftPlotDiagram();
            DevExpress.XtraCharts.Series series25 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView31 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.Series series26 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView32 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.Series series27 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView33 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.Series series28 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView34 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            DevExpress.XtraCharts.SwiftPlotSeriesView swiftPlotSeriesView35 = new DevExpress.XtraCharts.SwiftPlotSeriesView();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotDiagram7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // chartControl1
            // 
            this.chartControl1.AppearanceNameSerializable = "Nature Colors";
            swiftPlotDiagram7.AxisX.Range.AlwaysShowZeroLevel = true;
            swiftPlotDiagram7.AxisX.Range.ScrollingRange.SideMarginsEnabled = true;
            swiftPlotDiagram7.AxisX.Range.SideMarginsEnabled = true;
            swiftPlotDiagram7.AxisX.VisibleInPanesSerializable = "-1";
            swiftPlotDiagram7.AxisY.Range.AlwaysShowZeroLevel = true;
            swiftPlotDiagram7.AxisY.Range.ScrollingRange.SideMarginsEnabled = true;
            swiftPlotDiagram7.AxisY.Range.SideMarginsEnabled = true;
            swiftPlotDiagram7.AxisY.VisibleInPanesSerializable = "-1";
            this.chartControl1.Diagram = swiftPlotDiagram7;
            this.chartControl1.Location = new System.Drawing.Point(486, 8);
            this.chartControl1.Name = "chartControl1";
            series25.Name = "Series 1";
            series25.View = swiftPlotSeriesView31;
            series26.Name = "Series 2";
            series26.View = swiftPlotSeriesView32;
            series27.Name = "Series 3";
            series27.View = swiftPlotSeriesView33;
            series28.Name = "Series 4";
            series28.View = swiftPlotSeriesView34;
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series25,
        series26,
        series27,
        series28};
            this.chartControl1.SeriesTemplate.View = swiftPlotSeriesView35;
            this.chartControl1.Size = new System.Drawing.Size(776, 945);
            this.chartControl1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(576, 107);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::EarthquakeSimulationAPP.Properties.Resources.creek_vector_ripple_6;
            this.pictureBox2.Location = new System.Drawing.Point(329, 631);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(42, 55);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::EarthquakeSimulationAPP.Properties.Resources.Capture;
            this.pictureBox1.Location = new System.Drawing.Point(-1, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(487, 935);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::EarthquakeSimulationAPP.Properties.Resources.creek_vector_ripple_6;
            this.pictureBox3.Location = new System.Drawing.Point(329, 373);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(42, 55);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 4;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Visible = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::EarthquakeSimulationAPP.Properties.Resources.creek_vector_ripple_6;
            this.pictureBox4.Location = new System.Drawing.Point(33, 317);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(42, 55);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 5;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Visible = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::EarthquakeSimulationAPP.Properties.Resources.creek_vector_ripple_6;
            this.pictureBox5.Location = new System.Drawing.Point(33, 631);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(42, 55);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 6;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 700;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 934);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.chartControl1);
            this.Name = "Form1";
            this.Text = "Earthquake  Statistics Tequila";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(swiftPlotDiagram7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(swiftPlotSeriesView35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraCharts.ChartControl chartControl1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Timer timer1;
    }
}

